<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class EscolaridadeModel extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function insert($dados) {
        $this->db->insert('escolaridade', $dados);
        $id = $this->db->insert_id();
        if($id>0){
            return $id;
        }else{
            return FALSE;
        }
    }
}