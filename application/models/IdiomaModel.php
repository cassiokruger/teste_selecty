<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class IdiomaModel extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function insert($dados) {
        $this->db->insert('idioma', $dados);
        $id = $this->db->insert_id();
        if($id>0){
            return $id;
        }else{
            return FALSE;
        }
    }
}