<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class CargoModel extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function get($id=NULL) {
        if($id == NULL){
            return $this->db->get('cargo')->result();
        }else{
            $this->db->where('id', $id);
            return $this->db->get('cargo')->result();
        }
    }

    public function insert($dados) {
        $this->db->insert('cargo', $dados);
        $id = $this->db->insert_id();
        if($id>0){
            return $id;
        }else{
            return FALSE;
        }
    }
}