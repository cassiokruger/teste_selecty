<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class EnvolvimentoModel extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function insert($dados) {
        $this->db->insert('envolvimento', $dados);
        $id = $this->db->insert_id();
        if($id>0){
            return $id;
        }else{
            return FALSE;
        }
    }
}