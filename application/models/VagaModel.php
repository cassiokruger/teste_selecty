<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class VagaModel extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function get($id=NULL) {
        if($id == NULL){
            $this->db->select('v.id, c.nome AS cargo, v.salario, p.nome AS responsavel, v.data_criacao');
            $this->db->from('vaga v');
            $this->db->join('cargo c', 'v.cargo_id = c.id');
            $this->db->join('envolvimento e', 'e.vaga_id = v.id');
            $this->db->join('pessoa p', 'p.id = e.pessoa_id');
            $this->db->where('e.envolvimento', 1);
            return $this->db->get()->result();
        }else{
            $this->db->select('v.id, c.nome AS cargo, v.salario, p.nome AS responsavel, v.data_criacao');
            $this->db->from('vaga v');
            $this->db->join('cargo c', 'v.cargo_id = c.id');
            $this->db->join('envolvimento e', 'e.vaga_id = v.id');
            $this->db->join('pessoa p', 'p.id = e.pessoa_id');
            $this->db->where('e.envolvimento', 1);
            $this->db->where('id', $id);
            return $this->db->get()->result();
        }
    }

    public function insert($dados) {
        $this->db->insert('vaga', $dados);
        $id = $this->db->insert_id();
        if($id>0){
            return $id;
        }else{
            return FALSE;
        }
    }

    public function delete($id){
        return ($this->db->delete('vaga', array('id' => $id))) ? TRUE : FALSE;
    }
}