<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class PessoaModel extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function insert($dados) {
        $this->db->insert('pessoa', $dados);
        $id = $this->db->insert_id();
        if($id>0){
            return $id;
        }else{
            return FALSE;
        }
    }
}