<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Nova Vaga</title>

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
    <script>tinymce.init({selector:'textarea'});</script>

    <style>
        body {
            padding-top: 50px;
        }
        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>
</head>
<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Teste Selecty</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url(); ?>index.php/Vagas">Vagas</a></li>
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Vagas/new">Nova vaga</a></li>
        </ul>
        </div><!--/.nav-collapse -->
    </div>
    </nav>

    <div class="container">
        
        <div class="row">
            <h1>Cadastrar nova vaga</h1>

            <form action="<?php echo base_url(); ?>index.php/Vagas/save" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">Envolvidos na Vaga</div>
                    <div class="panel-body">
                        <div class="col-md-4 panel panel-default after-add-more">
                            <div class="panel-body">
                            <button class="btn btn-success btn-xs pull-right add-more" type="button" data-toggle="tooltip" data-placement="top" title="Novo envolvimento"><i class="glyphicon glyphicon-plus"></i></button>
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control" name="nome[]" id="nome" placeholder="Nome">
                                </div>
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="email" class="form-control" name="email[]" id="email" placeholder="E-mail">
                                </div>
                                <div class="form-group">
                                    <label for="envolvimento">Envolvimento</label>
                                    <select class="form-control" name="envolvimento[]" id="envolvimento">
                                        <option value="1">Responsavel</option>
                                        <!-- <option value="2">Requerente</option>
                                        <option value="3">Interessado</option> -->
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Dados básicos da vaga</div>
                    <div class="panel-body">
                        <label for="cargo">Cargo</label>
                        <div class="input-group">
                            <select class="form-control " name="cargo" id="cargo">
                                <?php
                                    foreach ($cargos as $key => $value) {
                                        echo '<option value="'.$value->id.'">'.$value->nome.'</option>';
                                    }
                                ?>
                            </select>
                            <span class="input-group-btn">
                                <a href="" class="btn btn-success" data-toggle="modal" data-target="#novoCargo" title="Adicionar Cargo"><span class="glyphicon glyphicon-plus"></span></a>
                            </span>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="nivel_vaga">Nivel</label>
                            <select class="form-control " name="nivel_vaga" id="nivel_vaga">
                                <option value="1">Operacional</option>
                                <option value="2">Tecnico</option>
                                <option value="3">Analista</option>
                                <option value="4">Coordenacao</option>
                                <option value="5">Gerencia</option>
                                <option value="6">Diretoria</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="area">Area</label>
                            <select class="form-control " name="area" id="area">
                                <option value="1">TI</option>
                                <option value="2">Administrativo</option>
                                <option value="3">Financeiro</option>
                                <option value="4">Comercial</option>
                                <option value="5">Marketing</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="salario">Salario</label>
                            <input type="number" step="any" class="form-control" name="salario" id="salario" placeholder="Salario" onkeyup="verificarNivel()">
                        </div>
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <textarea name="descricao" id="descricao" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Perfil do candidato desejado</div>
                    <div class="panel-body">
                        <h3>Escolaridade</h3>
                            <div class="form-group">
                                <label for="nivel_escolaridade">Nivel</label>
                                <select class="form-control " name="nivel_escolaridade" id="nivel_escolaridade">
                                    <option value="1">Basico</option>
                                    <option value="2">Intermediario</option>
                                    <option value="3">Avancado</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="curso">Curso</label>
                                <input type="text" class="form-control" name="curso" id="curso" placeholder="Curso">
                            </div>
                            <div class="form-group">
                                <label for="instituicao">Instituição</label>
                                <input type="text" class="form-control" name="instituicao" id="instituicao" placeholder="Instituição">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control " name="status" id="status">
                                    <option value="1">Finalizado</option>
                                    <option value="2">Em andamento</option>
                                    <option value="3">Trancado</option>
                                </select>
                            </div>

                        <h3>Idioma</h3>
                            <div class="form-group">
                                <label for="idioma">Idioma</label>
                                <input type="text" class="form-control" name="idioma" id="idioma" placeholder="Idioma">
                            </div>
                            <div class="form-group">
                                <label for="nivel_idioma">Nivel</label>
                                <select class="form-control " name="nivel_idioma" id="nivel_idioma">
                                    <option value="1">Basico</option>
                                    <option value="2">Intermediario</option>
                                    <option value="3">Avancado</option>
                                </select>
                            </div>
                    </div>
                </div>

                <button class="btn btn-success" type="submit">Salvar</button>
            </form>
        </div>

    </div><!-- /.container -->

<!-- ADICIONA CAMPO DE ENVOLVIMENTO -->
<div class="copy-fields hide"> 
  <div class="col-md-4 panel panel-default">
    <div class="panel-body">
        <button class="btn btn-danger btn-xs pull-right remove" type="button" data-toggle="tooltip" data-placement="top" title="Remover contato"><i class="glyphicon glyphicon-remove"></i></button>

        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" name="nome[]" id="nome" placeholder="Nome">
        </div>
        <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" name="email[]" id="email" placeholder="E-mail">
        </div>
        <div class="form-group">
        <label for="envolvimento">Envolvimento</label>
            <select class="form-control" name="envolvimento[]" id="envolvimento">
                <!-- <option value="1">Responsavel</option> -->
                <option value="2">Requerente</option>
                <option value="3">Interessado</option>
            </select>
        </div>
    </div>
  </div><!-- fim col-4-->
</div>
<!-- FIM DO ADICIONA CAMPO DE ENVOLVIMENTO -->

<!-- Modal novoCargo -->
<div class="modal fade" id="novoCargo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Adicionar Cargo</h4>
            </div>
            <div class="modal-body">
                <?php
                    echo form_label('Novo Cargo', 'novo_cargo');
                    echo form_input(array('name'=>'novo_cargo', 'id'=>'novo_cargo','class'=>'form-control'));
                ?>
            </div>
            <div class="modal-footer">
                <?php
                    echo form_button(array('class'=>'btn btn-default', 'data-dismiss'=>'modal', 'content'=>'Cancelar'));
                    echo form_button(array('class'=>'btn btn-primary', 'data-dismiss'=>'modal', 'content'=>'Salvar', 'onclick'=>'inserirNovoCargo()'));
                ?>
            </div>
        </div>
    </div>
</div>

</body>

<script>
    $(document).ready(function() {
        //here first get the contents of the div with name class copy-fields and add it to after "after-add-more" div class.
        $(".add-more").click(function(){
            var html = $(".copy-fields").html();
            $(".after-add-more").after(html);
        });

        //here it will remove the current value of the remove button which has been pressed
        $("body").on("click",".remove",function(){ 
            $(this).parents(".col-md-4").remove();
        });
    })

    // Adiciona Novo Cargo
    function inserirNovoCargo(){
        var cargo = $("#novo_cargo").val();
        
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Cargos/save",
            dataType: 'json',
            data: {
                cargo: cargo
            },
            success: function(data) {
                if(data.retorno == "Erro"){
                    alert(data.msg);
                }else{
                    //alert(data.msg);
                    carregaComboboxCargos(data.cargo);
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert("Erro: " + xhr.status);
            }
        });
    }

    // Carrega Combobox de Cargos
    function carregaComboboxCargos(cargo) {
        var comboCargos = document.getElementById("cargo");

        //remove todos os options do select
        while (comboCargos.length) {
            comboCargos.remove(0);
        }
        
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/Cargos/show",
            type: "POST",
            dataType: "json",
            beforeSend: function () {
                $("#cargo").html('<option value="0">Aguarde...</option>');
            },
            success: function(cargos){
                comboCargos.remove(0);

                $.each(cargos, function(){
                    var option = "";
                    if (this.id == cargo){
                        option += '<option value=' + this.id + ' selected>' + this.nome + '</option>';
                    }else{
                        option += '<option value=' + this.id + '>' + this.nome + '</option>';
                    }
                    
                    $("#cargo").append(option);
                })

                // Limpa input do novo cargo
                $("#novo_cargo").val('');
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert("Erro: " + xhr.status);
            }
        })
    }

    function verificarNivel(){
        var nivel = $('#nivel_vaga option:selected').val();
        var salario = $('#salario').val();
        if(nivel < 4){
            if(salario > 5000){
                $('#salario').val('');
                $('#salario').focus();
                alert("Salario não pode ser maior que R$ 5.000,00 para vagas abaixo do nivel Coordenação");
            }
        }
    }
</script>

</html>