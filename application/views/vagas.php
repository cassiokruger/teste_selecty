<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Vagas</title>

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
    <script>tinymce.init({selector:'textarea'});</script>

    <!-- Datatable -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></script>
    <script src="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#tableVagas').DataTable();
        } );
    </script>

    <style>
        body {
            padding-top: 50px;
        }
        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>
</head>
<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Teste Selecty</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Vagas">Vagas</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Vagas/new">Nova vaga</a></li>
        </ul>
        </div><!--/.nav-collapse -->
    </div>
    </nav>

    <div class="container">
        
        <div class="row">
            <h1>Vagas</h1>
            <div class="text-right"><a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/Vagas/new">Nova vaga</a></div>
            <table id="tableVagas" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>cargo</th>
                        <th>salario</th>
                        <th>responsavel</th>
                        <th>data criação</th>
                        <th>excluir</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($vagas as $key => $value) {
                            echo "<tr><td>";
                            echo $value->id;
                            echo "</td><td>";
                            echo $value->cargo;
                            echo "</td><td>";
                            echo $value->salario;
                            echo "</td><td>";
                            echo $value->responsavel;
                            echo "</td><td>";
                            echo $value->data_criacao;
                            echo "</td><td>";
                            echo '<a href="'.base_url().'index.php/Vagas/delete/'.$value->id.'" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                            echo "</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>

    </div><!-- /.container -->

</body>

</html>