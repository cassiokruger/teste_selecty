<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vagas extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('VagaModel','',TRUE);
		$this->load->model('PessoaModel','',TRUE);
		$this->load->model('EscolaridadeModel','',TRUE);
		$this->load->model('IdiomaModel','',TRUE);
		$this->load->model('EnvolvimentoModel','',TRUE);
		$this->load->model('CargoModel','',TRUE);

		$this->load->library('email');
	}

	public function index()
	{
		$dados['vagas'] = $this->VagaModel->get();
		$this->load->view('vagas', $dados);
	}

	public function new()
	{
		$dados['cargos'] = $this->CargoModel->get();
		$this->load->view('nova_vaga', $dados);
	}

	public function save()
	{
		// ADICIONA ESCOLARIDADE
		$escolaridade = array(
			'nivel' => $this->input->post('nivel_escolaridade'),
			'curso' => $this->input->post('curso'),
			'instituicao' => $this->input->post('instituicao'),
			'status' => $this->input->post('status')
		);
		$escolaridade_id = $this->EscolaridadeModel->insert($escolaridade);

		// ADICIONA IDIOMA
		$idioma = array(
			'idioma' => $this->input->post('idioma'),
			'nivel' => $this->input->post('nivel_idioma')
		);
		$idioma_id = $this->IdiomaModel->insert($idioma);

		// ADICIONA VAGA
		$vaga = array(
			'data_criacao' => date('Y-m-d'),
			'cargo_id' => $this->input->post('cargo'),
			'nivel' => $this->input->post('nivel_escolaridade'),
			'area' => $this->input->post('area'),
			'salario' => $this->input->post('salario'),
			'descricao' => $this->input->post('descricao'),
			'escolaridade_id' => $escolaridade_id,
			'idioma_id' => $idioma_id
		);
		$vaga_id = $this->VagaModel->insert($vaga);

		// ADICIONA PESSOA E ENVOLVIMENTO
		$nome = $this->input->post('nome');
		$email = $this->input->post('email');
		$envolvimento = $this->input->post('envolvimento');

		foreach ($envolvimento as $key => $value) {
			// Insiro cada pessoa no banco e retorno o id dela
			$pessoa_id = $this->PessoaModel->insert(array('nome'=>$nome[$key], 'email'=>$email[$key]));
			
			// Adiciono o envolvimento dessa pessoa com a vaga
			$envolvimento = array(
				'vaga_id' => $vaga_id,
				'pessoa_id' => $pessoa_id,
				'envolvimento' => $value
			);
			$this->EnvolvimentoModel->insert($envolvimento);
		}

		//Envia email para o responsavel
		$config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'teste.selecty@gmail.com';
        $config['smtp_pass']    = 'abacaxi123';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'text'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from('teste.selecty@gmail.com', 'Cassio');
        $this->email->to($email[0]); // Email do responsavel sempre é o zero 

        $this->email->subject('Vaga criada com sucesso');
        $this->email->message('A nova vaga foi criada com sucesso.');  

        $this->email->send();

		//Carrega a view
		$dados['vagas'] = $this->VagaModel->get();
		$this->load->view('vagas', $dados);
	}

	public function delete($id)
	{
		$this->VagaModel->delete($id);
		
		$dados['vagas'] = $this->VagaModel->get();
		$this->load->view('vagas', $dados);
	}
}
