<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargos extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('CargoModel','',TRUE);
	}

	public function show($id=NULL)
	{
		echo json_encode($this->CargoModel->get($id));
	}

	public function save()
	{
		$dados = array(
			'nome' => $this->input->post('cargo')
		);

		$inserir = $this->CargoModel->insert($dados);

		if($inserir > 0)
		{
			echo '{
					"retorno" : "Sucesso",
					"msg" : "Cargo adicionado com sucesso!",
					"cargo" : "'.$inserir.'"
				}';
		}
		else
		{
			echo '{
					"retorno" : "Erro",
					"msg" : "Erro ao adicionar cargo!"
				}';
		}
	}
}
