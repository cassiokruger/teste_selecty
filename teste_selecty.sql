-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 30, 2019 at 06:33 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teste_selecty`
--

-- --------------------------------------------------------

--
-- Table structure for table `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cargo`
--

INSERT INTO `cargo` (`id`, `nome`) VALUES
(1, 'Desenvolvedor');

-- --------------------------------------------------------

--
-- Table structure for table `envolvimento`
--

CREATE TABLE `envolvimento` (
  `id` int(11) NOT NULL,
  `vaga_id` int(11) DEFAULT NULL,
  `pessoa_id` int(11) DEFAULT NULL,
  `envolvimento` tinyint(4) DEFAULT NULL COMMENT '1=responsavel, 2=requerente, 3=interessado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `envolvimento`
--

INSERT INTO `envolvimento` (`id`, `vaga_id`, `pessoa_id`, `envolvimento`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 2),
(3, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `escolaridade`
--

CREATE TABLE `escolaridade` (
  `id` int(11) NOT NULL,
  `nivel` tinyint(4) DEFAULT NULL COMMENT '1=basico, 2=intermediario, 3=avancado',
  `curso` varchar(255) DEFAULT NULL,
  `instituicao` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1=finalizado, 2=em andamento, 3=trancado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `escolaridade`
--

INSERT INTO `escolaridade` (`id`, `nivel`, `curso`, `instituicao`, `status`) VALUES
(1, 3, 'ADS', 'Faculdade', 1),
(2, 2, 'teste', 'teste', 1);

-- --------------------------------------------------------

--
-- Table structure for table `idioma`
--

CREATE TABLE `idioma` (
  `id` int(11) NOT NULL,
  `idioma` varchar(255) NOT NULL,
  `nivel` tinyint(4) NOT NULL COMMENT '1=basico, 2=intermediario, 3=avancado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `idioma`
--

INSERT INTO `idioma` (`id`, `idioma`, `nivel`) VALUES
(1, 'Ingles', 2),
(2, 'teste', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pessoa`
--

CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pessoa`
--

INSERT INTO `pessoa` (`id`, `nome`, `email`) VALUES
(1, 'Cássio', 'cassio@email.com'),
(2, 'Andressa', 'andressa@email.com'),
(3, 'Cassio', 'cassiokruger@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `vaga`
--

CREATE TABLE `vaga` (
  `id` int(11) NOT NULL,
  `data_criacao` date DEFAULT NULL,
  `cargo_id` int(11) DEFAULT NULL,
  `nivel` tinyint(4) DEFAULT NULL COMMENT '1=operacional, 2=tecnico, 3=analista, 4=coordenacao, 5=gerencia, 6=diretoria',
  `area` tinyint(4) DEFAULT NULL COMMENT '1=ti, 2=administrativo, 3=financeiro, 4=comercial, 5=marketing',
  `salario` decimal(6,0) DEFAULT NULL,
  `descricao` text,
  `escolaridade_id` int(11) DEFAULT NULL,
  `idioma_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vaga`
--

INSERT INTO `vaga` (`id`, `data_criacao`, `cargo_id`, `nivel`, `area`, `salario`, `descricao`, `escolaridade_id`, `idioma_id`) VALUES
(1, '2019-06-30', 1, 3, 1, '5000', '<p>teste<strong>teste<em>teste</em></strong></p>', 1, 1),
(2, '2019-06-30', 1, 2, 1, '4500', '<p>teste<strong>teste<em>teste</em></strong></p>', 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `envolvimento`
--
ALTER TABLE `envolvimento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `escolaridade`
--
ALTER TABLE `escolaridade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `idioma`
--
ALTER TABLE `idioma`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vaga`
--
ALTER TABLE `vaga`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `envolvimento`
--
ALTER TABLE `envolvimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `escolaridade`
--
ALTER TABLE `escolaridade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `idioma`
--
ALTER TABLE `idioma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vaga`
--
ALTER TABLE `vaga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
